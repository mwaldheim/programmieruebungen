// Laden von Module readline für Abruffunktion der Eingabe
// https://nodejs.org/api/readline.html
const readline = require('readline');

// Interface für Input / Output erstellt, da sonst keine Eingabe und Ausgabe erfolgt
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Färben von Console (Farbwerte)
const R = 31;
const G = 32;
const Y = 33;
const B = 36;

// Konstanten für Werte der Schere / Stein / Papier Abgleiche
const SCHERE = 0;
const PAPIER = 1;
const STEIN = 2;
const ECHSE = 3;
const SPOCK = 4;
const ERROR = -1;

// Array für die Ausgabe der Werte als Worte
const arrNamen = ["Schere", "Papier", "Stein", "Echse", "Spock"];
// Array min den Wins aus Sicht des Computers
const arrWins = [
    [false, true, false, true, false], // Schere: Gewinnt gegen Papier und Echs
    [false, false, true, false, true], // Papier: Gewinnt gegen Stein und Spock
    [true, false, false, true, false], // Stein: Gewinnt gegen Schere und Echse
    [false, true, false, false, true], // Echse: Gewinnt gegen Papier und Spock
    [true, false, true, false, false]  // Spock: Gewinnt gegen Schere und Stein
];
// Variable für Auswahl Benutzer & Computer
let intUserValue = 0;
let intCompValue = 0;

/**
 * Funktion für Schere Stein Papier (SSP)
 * @constructor
 */
let SSP = function () {
    // Mit readline-Instanz eine Frage stellen und die Antwort als Callback erhalten
    rl.question('Bitte Schere (S/1), Papier (P/2), Stein (K/3), Echse (E/4), Spock (F/5), Quit (Q/0) eingeben: ', (answer) => {
        // Pausieren von Eingabe
        rl.pause();
        // Holen von erstem Zeichen und dessen Umwandlung in Großbuchstaben
        switch (answer.charAt(0).toUpperCase()) {
            case "S":
            case "1":
                // Auswahl von Schere
                intUserValue = SCHERE;
                break;
            case "K":
            case "3":
                // Auswahl von (Kiesel)-Stein
                intUserValue = STEIN;
                break;
            case "P":
            case "2":
                // Auswahl von Papier
                intUserValue = PAPIER;
                break;
            case "E":
            case "4":
                // Auswahl von Echse
                intUserValue = ECHSE;
                break;
            case "F":
            case "5":
                // Auswahl von Spock
                intUserValue = SPOCK;
                break;
            default:
                // Abfangen von nicht gehandelter Eingabe
                intUserValue = ERROR;
                break;
            case "Q":
            case "0":
                // Schließen von readline und beenden von Funktion durch return
                console.error("\x1b["+R+"m%s\x1b[0m","Programm beendet");
                return rl.close();
        }
        // Wenn Wert eine plausible Eingabe ist
        if (intUserValue >= 0) {
            // Holen von Random-Wert aus der Math-Funktion mit dem Limit von 5 (0 - 4)
            intCompValue = Math.floor(Math.random() * Math.floor(5));
            console.log("\x1b["+B+"m%s\x1b[0m","Computer hat: " + arrNamen[intCompValue] + " User hat: " + arrNamen[intUserValue]);
            // Abgleich von Werten, wenn identisch
            if (intCompValue === intUserValue) {
                console.log("Unentschieden");
            } else if (arrWins[intCompValue][intUserValue]) { // Gewinntabelle aus Sicht des Computers gestartet
                console.log("\x1b["+Y+"m%s\x1b[0m","Computer gewinnt.");
            } else {
                console.log("\x1b["+G+"m%s\x1b[0m","User gewinnt.");
            }
        } else {
            // Eingabe nicht plausibel
            console.error("\x1b["+R+"m%s\x1b[0m","Error: Eingabe nicht plausibel");
        }
        // Rekursiver Aufruf der SSP-Fuunktion für weitere Runde
        SSP();
    });
};
// Start der Funktion
SSP();
