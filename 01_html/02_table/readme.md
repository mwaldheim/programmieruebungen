# HTML-Übung 02: Tabellen
Eine einfache Art der Strukturierten Ausgabe von Daten ist die Tabelle. Hierbei geht es um die Grundlagen und Möglichkeiten von Tabellen

# Aufgabe
Die Aufgabe ist es, eine Tabelle aufzubauen, welche 6 Spalten hat. Oben ist der Kopf-Bereich mit den Inhalten **Header 1** - 6. Darin sind im Körper der Tabelle 4 Zeilen mit den jeweiligen Datensätzen. Die Zeile 2 wird an der 5. Stelle ein zweispaltiges Feld enthalten sein. In der 3. Zeile geht die 4 Zelle über zwei Zeilen. Die Tabelle wird durch eine Fußzeile mit den Inhalten **Footer 1** - 6 befüllt.

Zudem soll die Tabelle nicht die innerhalb der Form bleiben und keine Zelle darf zu weit raus stehen. Damit die Zellen erkennbar ist, empfehle ich die Verwendung von CSS im den Rahmen der Daten-Sätze für Kopf, Körper und Fuß hervor zu heben. Die Rahmenstärke soll 1 Pixel betragen und Schwarz sein.

Die Tabelle soll anschließend so aussehen (Beispiel in _Google Chrome_ gemacht)

![Tabelle als Muster](tabelle.png)

# Ziel der Aufgabe
1. Grundaufbau von HTML-Tabellen
1. Spannweiten von Zellen anpassen
1. Inline-Styling innerhalb der Tabelle

# Hilfsmaterialien
- [Grundlagen der Tabelle](https://www.w3schools.com/html/html_tables.asp)
- [CSS innerhalb einer HTML](https://www.w3schools.com/html/html_css.asp)
