# HTML-Übung 01: Formulare
Einer der wichtigsten HTML-Grundlagen ist der Aufruf von Formularen, die vorallem in Verwendung mit Javascript und PHP gerne in Anwendung kommen. Eingabefelder sind hierbei ein wichtiger Punkt.

# Aufgabe
Die Aufgabe ist es, ein funktionierendes Formular zu bauen, worin zwei Eingabefelder für einen Benutzernamen und Passwort-Feld enthält und einen Button zum absenden des Formulars.

Wenn das Formular funktioniert, müsste nun folgendes in der URL stehen:
`index.html?user=Test&pwd=test`

# Ziel der Aufgabe
1. Verstehen der HTML-Grundlagen (Grundaufbau + Elemente und Attribute)
1. Aufbau eines Formulars inkl. Felder und Buttons
1. Verstehen, wie die URL als Ergebnis nach dem Absenden sich zusammen setzt.

# Hilfsmaterialien
- [Grundaufbau von HTML](https://www.w3schools.com/html/default.asp)
    - [Basics](https://www.w3schools.com/html/html_basic.asp)
    - [Elements](https://www.w3schools.com/html/html_elements.asp)
    - [Attrbutes](https://www.w3schools.com/html/html_attributes.asp)
- [Formular und Eingabefelder](https://www.w3schools.com/html/html_forms.asp)
    - [Form Elements](https://www.w3schools.com/html/html_form_elements.asp)
    - [Input-Types](https://www.w3schools.com/html/html_form_input_types.asp)
    - [Input-Attributes](https://www.w3schools.com/html/html_form_attributes.asp)
    - [Input-Form-Attributes](https://www.w3schools.com/html/html_form_attributes_form.asp)
