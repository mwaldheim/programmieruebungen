# JS-Übung 02: Tabellenbefüllung
Es soll ein Formular gebaut werden, welches eine Tabelle befüllt, bearbeitet und leeren kann.

# Aufgabe
Da bereits alle HTML-Relevanten informationen durchgeführt wurden, wird dieser komplett bereit gestellt. Nun muss die komplette Logik für das Javascript geschrieben werden.

**Funktion Schreiben:**
-
Diese Funktion schreibt einen neuen Eintrag. Hierzu muss ein Index geführt werden, der uns eine eindeutige Zeile ermöglicht. Dieser Index wird beim Schreiben nach oben gezählt. Es muss der Wert des Formulares geholt werden. Anschließend muss die Zeile gebaut werden. Diese Zeile besteht aus drei Tabellen-Spalten mit insgesamt zwei Buttons in der ersten und letzten Spalte (load & delete). Diese Button sind später wichtig, um das Laden und Löschen der Zeile zu ermöglichen. Die mittlere Zeile beinhaltet unsere eingabe.

Die folgenden CSS-Klassen sollen verwendet werden:
- der TD des Lade-Buttons soll die class `load` haben.
- der TD des Textes soll die class `text` haben.
- der TD des Lösch-Buttons soll die class `del` haben.
- der Button zum laden hat den Text load und die CSS-Klassen `btn btn-primary`
- der Button zum löschen hat den Text delete und die CSS-Klassen `btn btn-danger`

Es muss unbedingt daran gedacht werden, den Buttons auch die richtige Funktion und deren korrekter Aufruf mitzugeben. Dies soll per EventListener auf einem `click` geschehen.

Die Row muss immer eindeutig indentifiziert werden, dazu gibt es den Index, der uns dabei hilft. Dieser muss jedoch ins HTML immer übertragen werden.

Jeder neue Eintrag muss in der Tabelle im `tbody` unten angehängt werden

**Funktion Laden:**
- 
Diese Funktion soll aus der gewählten Zeile den Text laden. Beim Ändern und speichern des Textes muss der Wert wieder an die gleiche Stelle geschrieben werden. Eine Variable hilft hierbei uns zu merken, woher der Eintrag kommt. Beim Abspeichern des Wertes sollte ein Fallback rein, wenn die Zeile nach dem Laden gelöscht wurde. Dann muss ein neuer Eintrag erstellt werden. Der QuerySelector gibt `null` zurück, wenn er nichts findet.

**Funktion Löschen:**
- 
Die ausgewählte Zeile wird komplett gelöscht. Ist es die letzte Zeile in der Tabelle, müsste diese mit dem Index identisch sein, sollte dass der Fall sein, kann der Index um eins reduziert werden.

# Ziel der Aufgabe
1. Erstellen von DOM-Elemenenten in Javascript
1. Manipulieren von CSS-Klassen per Javascript
1. HTML-Events beim aufruf bereits hinzufügen
1. Abfangen von Fehlverhalten des Codes
1. Löschen von DOM-Elementen aus dem Viewport
1. If-Bedingung


# Hilfsmaterialien
- [If-Bedingung](https://www.w3schools.com/js/js_if_else.asp)
- [Adding and Deleting Elements](https://www.w3schools.com/js/js_htmldom_document.asp)
- [CSS-Klassen per JS](https://developer.mozilla.org/de/docs/Web/API/Element/classList)
- [Attribute setzen](https://www.w3schools.com/jsref/met_element_setattribute.asp)
- [CSS-Selektoren für QuerySelector](https://www.w3schools.com/cssref/css_selectors.asp)
