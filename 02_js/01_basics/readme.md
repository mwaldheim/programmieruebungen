# JS-Übung 01: Basics für Javascript
Hierbei werden die Grundlagen von Javascript aufgebaut und demonstrativ verwendet.

# Aufgabe
Als erstes wird ein HTML benötigt, welches bereits von HTML kopiert wurde. Innerhalb des Dokuments sollen in den Kopf-Bereich die zwei verschiedenen Arten von Script-einbindungen durchgeführt werden.

**ACHTUNG: ES GIBT VIELE LÖSUNGSANSÄTZE, DIESE BASIEREND JEDOCH AUF EINE JQUERY-LÖSUNG. DA WIR JQUERY AUS UNSERE SOFTWARE LANGFRISTIG ENTFERNEN, WIRD VANILLA-SCRIPT HIER VERWENDET**

Vanilla-Script ist eine Art Rohform von Javascript, sprich ohne Frameworks wie z.B. JQuery. Mit ES6 wurde JQuery eh abgelöst.

Als erstes richten wir uns auf den Inline-Script. der ins HTML direkt eingebaut wird. Hier werden wir grundlegend definieren, wie Variablen definiert werden. Einmal der ES5-Standard und danach die zwei Variablentypen von ES6. Es ist wichtig beide Arten zu kennen, da deren Art davon abhängt, wie die Variablen später sichtbar sind.

Anschließend werden zwei Funktionen `ES5` und `ES6` gebaut. ES6 wird dabei direkt in eine Variable gepackt. In der Funktion `ES5` wird mittels Consolen-Ausgabe `0` ausgegeben. Die Funktion `ES6` gibt auf der Console `1` aus.

Anschließend wird dann ein EventListener auf das document geschrieben, der beide Funktionen startet, sobald das Document fertig geladen hat.

Nun geht es zu script 2. Dieses wird von der HTML ausgelagert. Das hat den Vorteil, der initiale Ladeprozess wird geringer.

Es wird wieder ein Event-Listener benötigt, welcher nach dem Laden des Dokuments eine Funktion startet, die sich alle Eingabefelder des HTML-Tags input zusammen sucht. Diese müssen dann einzeln durchgeschleift werden und pro Eingabe muss ein weiterer Event-Listener hinzugefügt werden, welcher bei jeder Eingabe das Element an eine Unterfunktion weiter gibt und dort in die Konsole den Value rein schreibt.

Der gesamte Code muss vollständig kommentiert sein, damit jede Passage erklärt ist.


# Ziel der Aufgabe
1. Wo kann Javascript platziert werden
1. Kommentar-Setzung in Javascript
1. Unterschiede zwischen ES5 und ES6
1. DOM-Elemente und deren Eventmöglichkeiten
1. Debuggen im Browser
1. Funktionen und Variablen
1. For-Schleife und Performance-Verbesserung


# Hilfsmaterialien
- [Where to](https://www.w3schools.com/js/js_whereto.asps)
- [Kommentare](https://www.w3schools.com/js/js_comments.asp)
- [Debuggin](https://www.w3schools.com/js/js_debugging.asp)
- [Variable ES5](https://www.w3schools.com/js/js_variables.asp)
- [Funktion ES5](https://www.w3schools.com/js/js_functions.asp)
- [Variablen und Arrow-Funktion ES6](https://www.w3schools.com/js/js_es6.asp)
- [DOM Elements](https://www.w3schools.com/js/js_htmldom_elements.asp)
- [DOM Events](https://www.w3schools.com/jsref/dom_obj_event.asp)
- [For-Schleifen](https://www.w3schools.com/js/js_loop_for.asp)
- [Performance](https://www.w3schools.com/js/js_performance.asp)
